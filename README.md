# **Converse.io**

![converse](https://image.ibb.co/dZNLh5/1.jpg)
![converse](https://image.ibb.co/jDbrpk/4.jpg)
![converse](https://image.ibb.co/ceu5FQ/6.jpg)

This project is focused on providing users a platform to  converse/chat in real time. 
The web application uses `Node.js` as a server side JavaScript execution environment coupled with `Express.js` 
as web application framework, `Angular 2` for frontend development and `Socket.io` as a JavaScript library for realtime sharing.



## Getting Started

To get you started you can simply clone the `Converse.io` repository and install the dependencies:

### Prerequisites

You need `git` to clone the `Converse.io` repository. You can get git from 
[here](https://git-scm.com/downloads)

We also use a number of Node.js tools to initialize the application. You must have Node.js
and its package manager (npm) installed. You can get them from 
[Node Package Manager](https://nodejs.org/)

### Clone `Converse.io`

Clone the `Converse.io` repository using git:

```
git clone https://bitbucket.org/jatinarr/converse.io.git
cd converse.io
```

If you just want to start a new project without the `Converse.io` commit history then you can do:

```
git clone --depth=1 https://bitbucket.org/jatinarr/converse.io.git <your-project-name>
cd <your-project-name>
```

The `depth=1` tells git to only pull down one commit worth of historical data.


### Install Dependencies

We get the tools we depend upon via `npm`, the 
[Node Package Manager](https://nodejs.org/)

Move to root directory of project-

cd ```<your-project-name>/npm install```

 
 
For `express & socket.io`
 
Move to server folder of project-

cd ```<your-project-name>/src/server/npm install```

The created folder `node_modules` - contains the npm packages for the tools we need

	


### Run the Application

The project is preconfigured with a simple development web server.

Start the Server: 
Move to `server` folder of project-

cd ``` <your-project-name>/src/server/node server```


Start the project/app:
Move to root directory of project-

cd ``` <your-project-name>/npm start```


``` Make sure that the internet connection is working fine ```

Now browse to the app at [localhost:3000/](localhost:3000/) for the first User
and keep opening the local hosts for consecutive users for real time converation.

`Note: Just in case an issue occurs try excuting the these #Backup steps.`

#### Backup (dependencies)

	Install typings:
	 
	 cd <your-project-name>/npm install -g typings
	 
	 
	 Install socket.io-client module:
	 
	 cd <your-project-name>/npm install socket.io-client --save
	 
	 cd <your-project-name>/typings install dt~socket.io-client --save --global


## Directory Layout

```
/
  src/
	app/                   --> all of the source files for the application
	  app.component.ts       --> default stylesheet
	  app.module.ts          --> root module, to bootstrap the app & contains all the imports 
	  app.routing.ts         --> routing module, handles the routes navigation
		components/            --> functionality related components
		  chat/                  --> main logical & UI component
			chat.component.ts      --> handles the logic, calls the required service
			chat.component.html    --> handles the UI part and the required bidirectional binding
		  navbar/               
			navbar.component.ts    --> navbar component
			navbar.component.html  --> navbar display 
		services/               -->  services to organize and share code across the app via dependency injection
		  chat.service.ts         --> handles connection, observables and messages
	server/ 			   --> all of the server files for the application
	  package.json	         --> holds metadata about the project & handle dependencies
	  server.js              --> holds server connection settings, port listening & get requests
	vendor/
	  socket.io-client/
	  socket.io.js 	         --> socket.io CDN (content delivery network) code
	
	index.html			   --> holds app loader logic, external links& scripts like bootstrap cdn
	styles.css			   --> obviously for styling the UI
```