"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var chat_service_1 = require("../../services/chat.service");
var ChatComponent = (function () {
    function ChatComponent(_chatService) {
        this._chatService = _chatService;
        this.messages = [];
        this.alert = false;
        this.myArr = ["“At the end of the day, One just want to sit with someone he/she likes and chat about what matters and even what doesn’t.”"];
    }
    ChatComponent.prototype.setUsername = function () {
        this._chatService.setUsername(this.username);
        this.alert = 'username set';
    };
    ChatComponent.prototype.createLeadArr = function () {
        var str1 = "“Stop thinking, Start talking.”";
        var str2 = "“Sometimes the greatest adventure is simply a converstion.”";
        var str3 = "“You know what's appealing? A real conversation.”";
        var str4 = "“Be brave enough to start a conversation that matters.”";
        var str5 = "“Effortless conversations are the best.”";
        this.myArr.push(str1);
        this.myArr.push(str2);
        this.myArr.push(str3);
        this.myArr.push(str4);
        this.myArr.push(str5);
        this.randomLead();
    };
    ;
    ChatComponent.prototype.randomLead = function () {
        var rand = this.myArr[Math.floor(Math.random() * this.myArr.length)];
        this.currString = rand;
    };
    ChatComponent.prototype.whoIsTyping = function () {
        this.currentTypist = this.username + " is typing . . .";
    };
    ChatComponent.prototype.endTyping = function () {
        this.currentTypist = "";
    };
    ChatComponent.prototype.sendMessage = function () {
        this._chatService.sendMessage(this.message, this.username);
        this.message = '';
    };
    ChatComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.createLeadArr();
        this.connection = this._chatService.getMsgs()
            .subscribe(function (message) {
            // console.log(message);
            // submitted the message and got it back
            // pushing the back returned msg to array for the view
            _this.messages.push(message);
        });
        //  this.connection = this._chatService.getMsgs()
        // .subscribe(function(msg) {
        //     console.log(msg);
        //     this.messages.push(msg)
        // });
    };
    ChatComponent.prototype.ngOnDestroy = function () {
        this.connection.unsubscribe();
    };
    return ChatComponent;
}());
ChatComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'chat',
        templateUrl: 'chat.component.html'
    }),
    __metadata("design:paramtypes", [chat_service_1.ChatService])
], ChatComponent);
exports.ChatComponent = ChatComponent;
//# sourceMappingURL=chat.component.js.map