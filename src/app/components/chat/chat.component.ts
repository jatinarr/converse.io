import { Component, OnInit, OnDestroy } from '@angular/core';
import {ChatService} from '../../services/chat.service';

@Component({
    moduleId:module.id,
    selector: 'chat',
    templateUrl: 'chat.component.html'
})
export class ChatComponent implements OnInit, OnDestroy { 
    messages: any=[];
    message: string;
    connection: any;
    username:string;
    currString:string;
    alert:any=false;
    currentTypist:string;
    myArr=["“At the end of the day, One just want to sit with someone he/she likes and chat about what matters and even what doesn’t.”"];
    
    constructor(private _chatService:ChatService){

    }
    

    setUsername(){

        this._chatService.setUsername(this.username);
        this.alert='username set';
    }


    createLeadArr(){
    var str1="“Stop thinking, Start talking.”";
    var str2="“Sometimes the greatest adventure is simply a converstion.”";
    var str3="“You know what's appealing? A real conversation.”";
    var str4="“Be brave enough to start a conversation that matters.”";
    var str5="“Effortless conversations are the best.”";

    this.myArr.push(str1);
    this.myArr.push(str2);
    this.myArr.push(str3);
    this.myArr.push(str4);
    this.myArr.push(str5);
    this.randomLead();
    
  };


   randomLead(){

    var rand =this.myArr[Math.floor(Math.random() * this.myArr.length)];
    this.currString=rand;
    

  }

  whoIsTyping(){

    this.currentTypist=this.username + " is typing . . .";

  }

  endTyping(){
      this.currentTypist="";
  }

    sendMessage(){
        this._chatService.sendMessage(this.message, this.username);
        this.message = '';
    }
    
    ngOnInit(){
        this.createLeadArr();
        this.connection = this._chatService.getMsgs()
        .subscribe(message => {
            // console.log(message);
            // submitted the message and got it back
            // pushing the back returned msg to array for the view
            this.messages.push(message);
            
        });
        

        //  this.connection = this._chatService.getMsgs()
        // .subscribe(function(msg) {
        //     console.log(msg);
        //     this.messages.push(msg)
        // });
    }
    
    ngOnDestroy(){
        this.connection.unsubscribe();
    }
}
