"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var github_service_1 = require("../../services/github.service");
var HomeComponent = (function () {
    function HomeComponent(_githubService) {
        this._githubService = _githubService;
        // this.pressed=false;
        this.goBtn = false;
        this.count = 0;
    }
    HomeComponent.prototype.go = function () {
        this.goBtn = true;
        this.searchUser();
    };
    HomeComponent.prototype.ClearFields = function () {
        //   var value = parseFloat((<HTMLInputElement>document.getElementById("searchField")).value);
        // var unit = (<HTMLInputElement>document.getElementById("searchField")).value; 
        document.getElementById("searchField").value = "";
    };
    HomeComponent.prototype.changePressed = function () {
        if (this.count == 1) {
        }
        else {
            // this.pressed=false;
            this.goBtn = false;
        }
        if (this.count == 0) {
            this.count = 1;
        }
        else
            this.count = 0;
        // console.log(this.count);
    };
    HomeComponent.prototype.searchUser = function () {
        var _this = this;
        if (this.count == 1) {
            this.searchUserDynamic();
        }
        else {
            if (this.goBtn) {
                this._githubService.updateUser(this.username);
                this._githubService.getUser()
                    .subscribe(function (res) {
                    _this.userDetails = res;
                });
                this._githubService.getRepos()
                    .subscribe(function (repo) {
                    _this.repos = repo;
                });
                this._githubService.getRepos()
                    .subscribe(function (repo) {
                    _this.repos = repo;
                });
            }
            this.goBtn = false;
        }
    };
    HomeComponent.prototype.searchUserDynamic = function () {
        var _this = this;
        this._githubService.updateUser(this.username);
        this._githubService.getUser()
            .subscribe(function (res) {
            _this.userDetails = res;
        });
        this._githubService.getRepos()
            .subscribe(function (repo) {
            _this.repos = repo;
        });
    };
    HomeComponent.prototype.visitBlog = function () {
        this.blog = 'http://';
        var regex = /http/;
        if (this.userDetails.blog.search(regex) == -1)
            return this.blog.concat(this.userDetails.blog);
        return this.userDetails.blog;
    };
    HomeComponent.prototype.createDate = function (oldDate) {
        var monthArr = ['January', 'February', 'March', 'April', 'May',
            'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        var day = oldDate.substr(8, 10);
        var month = oldDate.substr(5, 7);
        var year = oldDate.substr(0, 4);
        var newDate = day + ' ' + monthArr[parseInt(month) - 1] + ', '
            + year;
        return newDate;
    };
    return HomeComponent;
}());
HomeComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'home',
        templateUrl: 'home.component.html'
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof github_service_1.GitHubService !== "undefined" && github_service_1.GitHubService) === "function" && _a || Object])
], HomeComponent);
exports.HomeComponent = HomeComponent;
var _a;
//# sourceMappingURL=home.component.js.map