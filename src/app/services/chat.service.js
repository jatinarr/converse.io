"use strict";
var Observable_1 = require("rxjs/Observable");
var io = require("socket.io-client");
var ChatService = (function () {
    function ChatService() {
        this.url = 'http://localhost:8000';
    }
    ChatService.prototype.setUsername = function (username) {
        console.log('Username set: ' + username);
        // for storage using session storage
        sessionStorage.setItem('username', username);
    };
    ChatService.prototype.getUsername = function () {
        return sessionStorage.getItem('username');
    };
    // send mesg via service
    ChatService.prototype.sendMessage = function (message, username) {
        //send to all connected clients
        this.socket.emit('add-message', message, username);
        /* add-message is the event & data is the msg
        we are emiting the data to the add-message event
        in server.js, socket.on() method which then emits the msg back. */
    };
    // get messages via service
    ChatService.prototype.getMsgs = function () {
        var _this = this;
        var obs = new Observable_1.Observable(function (observer) {
            // passing the local url
            _this.socket = io(_this.url);
            //event listener,called on client to execute on server
            //socket.on('any event', function (data)){}
            _this.socket.on('message', function (data) {
                /*When an Observable produces values, it then informs
              the observer, calling .next() when a new value was
              successfully captured and .error() when an error occurs.*/
                observer.next(data);
            });
            return function () {
                _this.socket.disconnect();
            };
        });
        return obs;
    };
    return ChatService;
}());
exports.ChatService = ChatService;
//# sourceMappingURL=chat.service.js.map