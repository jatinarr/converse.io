import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';
import * as io from 'socket.io-client';

export class ChatService{

    private url = 'http://localhost:8000';
    private socket:any;


    setUsername(username:string){

        console.log('Username set: ' + username);
        // for storage using session storage

         sessionStorage.setItem('username', username);
    }

    getUsername(){

        return sessionStorage.getItem('username');
    }

    // send mesg via service
    sendMessage(message:string, username:string){

        //send to all connected clients
        this.socket.emit('add-message',message, username);
        /* add-message is the event & data is the msg
        we are emiting the data to the add-message event
        in server.js, socket.on() method which then emits the msg back. */
    }

    // get messages via service
    getMsgs() {

        let obs= new Observable((observer:any)=> {
            // passing the local url
            this.socket=io(this.url);
            
            //event listener,called on client to execute on server
            //socket.on('any event', function (data)){}
            this.socket.on('message', (data:any)=>{

        /*When an Observable produces values, it then informs 
      the observer, calling .next() when a new value was 
      successfully captured and .error() when an error occurs.*/
                observer.next(data);
            });

            return () => {
                this.socket.disconnect();
            }
        })
        return obs;
    }
}