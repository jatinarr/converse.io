var app=require('express')(); // we'll run that as a fn
var http=require('http').Server(app);

// createServer() is just a helper method for Server()

// instantiating socket.io attached to http server
var io=require('socket.io')(http);


app.get('/', function(req, res){

    res.sendfile('index.html');
})

// io.on event handler handles conn, discon events
//using socket object

//initial connection from a client.
io.on('connection', function(socket){
    console.log('User connected...');

    //Send a message when 
  setTimeout(function(){
	  //Sending an object when emmiting an event
      // will need a listener for this event in index.html
	socket.emit('testerEvent', { description: 'A custom event named testerEvent!'});
	}, 4000);


    //event listener
    socket.on('disconnect', function(){
        console.log('User disconnected...');
    })

    socket.on('add-message', function(message, username){

        //sending to all clients, include sender
        io.emit('message', {type: 'new-message', text:message, username: username})
    });

}); 

http.listen(8000,function (){
    console.log('Server running on port 8000...');
})